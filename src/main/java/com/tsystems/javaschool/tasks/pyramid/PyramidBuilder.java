package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    int numberOfRows;
    int numberOfElements;

    public int[][] buildPyramid(List<Integer> inputNumbers) {
         /**
         * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
         * from left to right). All vacant positions in the array are zeros.
         *
         * @param inputNumbers to be used in the pyramid
         * @return 2d array with pyramid inside
         * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
         */

        if (!isTriangle(inputNumbers)) throw new CannotBuildPyramidException();
        for (Integer check: inputNumbers){
            if (check==null) throw new CannotBuildPyramidException();
        }
        List<Integer> sortedInputNumbers = inputNumbers.stream().sorted().collect(Collectors.toList());
        int[][] pyramid = new int[numberOfRows][numberOfElements];

        int center = (numberOfElements/2);
        int count = 1;
        int indexOfElement = 0;
        int stepBack = 0;
        int start;

        for (int i = 0; i < numberOfRows; i++) {
            start = center - stepBack;
            for (int j = 0; j < count * 2; j +=2) {
                pyramid[i][start + j] = sortedInputNumbers.get(indexOfElement);
                indexOfElement++;
            }
            stepBack++;
            count++;
        }
        return pyramid;
    }

    private int getRowsNumber(List<Integer> inputNumbers){
        int arrSize = inputNumbers.size();
        int result = 0;
        for (int i = 0; i < arrSize; i++) {
            arrSize = arrSize-i;
            result++;
        }
        return result;
    }
    private boolean isTriangle(List<Integer> inputNumbers){
        numberOfRows = getRowsNumber(inputNumbers);
        numberOfElements = numberOfRows + (numberOfRows - 1);
        return inputNumbers.size() == numberOfRows * (numberOfRows +1)/2;
    }
}
